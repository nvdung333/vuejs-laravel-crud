<h3>Install VueJS</h3>
<pre>
npm install
npm install vue vue-router vue-axios --save
npm i vue-loader (to fix bug - if ERR while mix)
</pre>

<h3>Run one of the following commands to mix:</h3>
<pre>
npm run dev
npm run watch
npm run hot
</pre>

<h3>Run a command to set port:</h3>
<pre>
php artisan serve   (http://localhost:8000/)
(or) php artisan serve --port=1111  (http://localhost:1111/)
</pre>

<h3>Execute DUMP_SQL.sql in the database</h3>
