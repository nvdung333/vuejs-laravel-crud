<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/pr/index', 'App\Http\Controllers\PrsController@index');
Route::post('/pr/create', 'App\Http\Controllers\PrsController@store');
Route::get('/pr/edit/{pr_id}', 'App\Http\Controllers\PrsController@edit');
Route::post('/pr/update/{pr_id}', 'App\Http\Controllers\PrsController@update');
Route::delete('/pr/delete/{pr_id}', 'App\Http\Controllers\PrsController@destroy');
Route::get('/pr/search', 'App\Http\Controllers\PrsController@search');
