<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\PrsModel;
use App\Http\Resources\PrsCollection;


class PrsController extends Controller
{
    // INDEX
    public function index() {
        return new PrsCollection(PrsModel::all());
    }

    // CREATE
    public function store(Request $request) {

        $validatedData = $request->validate([
            'pr_name' => 'required',
            'pr_age' => 'required|numeric',
        ]);

        $pr = PrsModel::create($request->all());
        // $pr = new PrsModel([
        //     'pr_name' => $request->get('pr_name'),
        //     'pr_age' => $request->get('pr_age'),
        //     'pr_position' => $request->get('pr_position')
        // ]);
        // $pr->save();
        
        return response()->json('Created successfully!');
    }

    // EDIT
    public function edit($pr_id) {
        $pr = PrsModel::find($pr_id);
        return response()->json($pr);
    }

    // UPDATE
    public function update(Request $request, $pr_id) {

        $validatedData = $request->validate([
            'pr_name' => 'required',
            'pr_age' => 'required|numeric',
        ]);

        $pr = PrsModel::find($pr_id);
        $pr->update($request->all());
        return response()->json('Updated successfully!');
    }

    // DELETE
    public function destroy($pr_id) {
        $pr = PrsModel::find($pr_id);
        $pr->delete();
        return response()->json('Deleted successfully!');
    }

    //SEARCH
    public function search(Request $request) {
        $keyword = $request->kw;
        $data = PrsModel::where('pr_name', 'LIKE', '%'.$keyword.'%')->get();
        return response()->json($data);
    }
}
