<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrsModel extends Model
{
    use HasFactory;

    protected $table = 't_prs';
    protected $primaryKey = 'pr_id';
    protected $fillable = ['pr_name', 'pr_age', 'pr_position'];
    public $timestamps = false;

}
